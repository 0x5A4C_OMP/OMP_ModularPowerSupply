#include "stateApp.hpp"
#include "states.hpp"

#include "../../bsp/bsp.hpp"

void StateApp::onEnter(void)
{
}

void StateApp::process(void)
{
  Bsp::bspHeartBeat.toggle();

  Bsp::bspSevenSegDisp_U.setUnit(0b00111110);
  Bsp::bspSevenSegDisp_I.setUnit(0b00000110);

  Bsp::bspSevenSegDisp_U.setDot(1);
  Bsp::bspSevenSegDisp_I.setDot(0);

  Bsp::bspSevenSegDisp_U.setValue(Bsp::bspCurMon.getVoltage());
  Bsp::bspSevenSegDisp_I.setValue(Bsp::bspCurMon.getCurrent());
  Bsp::bspSevenSegDisp_U.display();
  Bsp::bspSevenSegDisp_I.display();
}

void StateApp::onExit(void)
{
}

StateApp stateApp;
