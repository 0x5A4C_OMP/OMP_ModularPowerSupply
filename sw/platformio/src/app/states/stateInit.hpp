#ifndef STATEAPP_HPP
#define STATEAPP_HPP

#include <Stm.hpp>

class StateInit: public St
{
public:
  StateInit(): St()
  {
  }

  virtual void onEnter(void);
  virtual void process(void);
  virtual void onExit(void);

protected:
private:
};

extern StateInit stateInit;

#endif
