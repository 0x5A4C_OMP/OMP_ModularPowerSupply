#include "stateInit.hpp"
#include "states.hpp"

#include "../../bsp/bsp.hpp"

void StateInit::onEnter(void)
{
  Bsp::bspHeartBeat.off();
}

void StateInit::process(void)
{
  stm->transitionTo((St*)&stateApp);
}

void StateInit::onExit(void)
{
}

StateInit stateInit;
