#ifndef STATEINIT_HPP
#define STATEINIT_HPP

#include <Stm.hpp>

class StateApp: public St
{
public:
  StateApp(): St()
  {
  }

  virtual void onEnter(void);
  virtual void process(void);
  virtual void onExit(void);

protected:
private:
};

extern StateApp stateApp;

#endif
