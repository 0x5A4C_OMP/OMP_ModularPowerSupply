#include "app.hpp"
#include <Stm.hpp>
#include "states/states.hpp"

Stm appStm;

void App::init(void)
{
  //states
  stateInit.setParentStm(&appStm);
  appStm.transitionTo((St*)&stateInit);
}

void App::process(void)
{
    appStm.process();
}
