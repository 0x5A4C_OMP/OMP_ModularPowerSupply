#ifndef APP_HPP
#define APP_HPP

namespace App
{
  void init(void);
  void process(void);
}

#endif
