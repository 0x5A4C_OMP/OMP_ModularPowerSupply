#include <Arduino.h>
#include <Arduino_FreeRTOS.h>
#include <semphr.h>  // add the FreeRTOS functions for Semaphores (or Flags).

#include "app/app.hpp"
#include "bsp/bsp.hpp"

#include "TM1637.h"

void taskBlink(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  // initialize digital LED_BUILTIN on pin 13 as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  for (;;) // A Task shall never return or exit.
  {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    vTaskDelay( 500 / portTICK_PERIOD_MS ); // wait for one second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    vTaskDelay( 500 / portTICK_PERIOD_MS ); // wait for one second
  }
}

void taskApp(void *pvParameters)
{
  (void) pvParameters;

  App::init();

  for (;;)
  {
    App::process();
    vTaskDelay( 300 / portTICK_PERIOD_MS );
  }
}

void taskBsp(void *pvParameters)
{
  (void) pvParameters;

  Bsp::init();

  for (;;)
  {
    Bsp::process();
    vTaskDelay( 2000 / portTICK_PERIOD_MS );
  }
}

void setup()
{
  Serial.begin(9600);

  while (!Serial)
  {
    ;
  }

  xTaskCreate(
    taskApp,
    (const portCHAR *)"App",   // A name just for humans
    256,  // This stack size can be checked & adjusted by reading the Stack Highwater
    NULL,
    5,  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    NULL );

    xTaskCreate(
      taskBsp,
      (const portCHAR *)"Bsp",   // A name just for humans
      256,  // This stack size can be checked & adjusted by reading the Stack Highwater
      NULL,
      5,  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
      NULL );

  // xTaskCreate(
  //   taskBlink,
  //   (const portCHAR *)"Blink",   // A name just for humans
  //   128,  // This stack size can be checked & adjusted by reading the Stack Highwater
  //   NULL,
  //   2,  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
  //   NULL );
}

void loop()
{
  // Empty. Things are done in Tasks.
}
