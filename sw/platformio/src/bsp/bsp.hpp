#ifndef BSP_HPP
#define BSP_HPP

#include "bspHeartBeat.hpp"
#include "bspSevenSegDisp.hpp"
#include "bspCurrentMon.hpp"

namespace Bsp
{
  extern BspHeartBeat bspHeartBeat;
  extern BspSevenSegDisp bspSevenSegDisp_U;
  extern BspSevenSegDisp bspSevenSegDisp_I;
  extern BspCurMon bspCurMon;

  void init(void);
  void process(void);
}

#endif
