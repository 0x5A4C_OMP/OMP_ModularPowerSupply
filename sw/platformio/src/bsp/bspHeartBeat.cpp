#include "bspHeartBeat.hpp"
#include <Arduino.h>

BspHeartBeat::BspHeartBeat()
{

}

void BspHeartBeat::init(void)
{
  pinMode(LED_BUILTIN, OUTPUT);
}

void BspHeartBeat::process(void)
{

}

void BspHeartBeat::on(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void BspHeartBeat::off(void)
{
  digitalWrite(LED_BUILTIN, LOW);
}

void BspHeartBeat::toggle(void)
{
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
