#ifndef BSP_HEARTBEAT_HPP
#define BSP_HEARTBEAT_HPP

class BspHeartBeat
{
public:
    BspHeartBeat();
    void init(void);
    void process(void);
    void on(void);
    void off(void);
    void toggle(void);
protected:
private:
};

#endif
