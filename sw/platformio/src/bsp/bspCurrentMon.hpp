#ifndef BSP_CURMON_HPP
#define BSP_CURMON_HPP

#include <stdint.h>
#include <Ina219Sens.hpp>

class BspCurMon
{
public:
    BspCurMon();
    void init(void);
    void process(void);
    uint32_t getCurrent(void);
    uint32_t getVoltage(void);
    uint32_t getPower(void);
protected:
private:
  Ina219Sens curMonitor;
};

#endif
