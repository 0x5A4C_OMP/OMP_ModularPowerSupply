#include "bspSevenSegDisp.hpp"
#include <Arduino.h>

BspSevenSegDisp::BspSevenSegDisp(const int clk, const int di): tm1637(TM1637Display(clk, di)), dotPos(0xff)
{
}

void BspSevenSegDisp::init(void)
{
  tm1637.setBrightness(2);
  data[0] = 0x00;
  data[1] = 0x00;
  data[2] = 0x00;
  data[3] = 0x00;
  tm1637.setSegments(data);
}

void BspSevenSegDisp::process(void)
{
}

void BspSevenSegDisp::display(void)
{
  if((dotPos >= 0) && (dotPos < 4))
  {
    data[dotPos] = data[dotPos] | 0b10000000;
  }
  tm1637.setSegments(data);
}

void BspSevenSegDisp::setValue(uint32_t value)
{
#if 0
  Serial.println(value);
  Serial.println(value / 100);
  Serial.println(tm1637.encodeDigit(value / 100));
  Serial.println((value % 100) / 10);
  Serial.println(tm1637.encodeDigit((value % 100) / 10));
  Serial.println((value % 100) % 10);
  Serial.println(tm1637.encodeDigit((value % 100) % 10));
  Serial.println("");
  Serial.println("");
#endif
  data[0] = tm1637.encodeDigit(value / 100);
  data[1] = tm1637.encodeDigit((value % 100) / 10);
  data[2] = tm1637.encodeDigit((value % 100) % 10);
}

void BspSevenSegDisp::setUnit(uint8_t unit)
{
  data[3] = unit;
}

void BspSevenSegDisp::setDot(uint8_t pos)
{
  dotPos = pos;
}
