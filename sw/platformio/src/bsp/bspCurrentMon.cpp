#include "bspCurrentMon.hpp"
#include <Arduino.h>
#include <INA219.h>

BspCurMon::BspCurMon()
{
}

void BspCurMon::init(void)
{
  curMonitor.begin(64);
  // config values (range, gain, bus adc, shunt adc, mode) can be derived from pp26-27 in the datasheet
  // defaults are:
  // range = 1 (0-32V bus voltage range)
  // gain = 3 (1/8 gain - 320mV range)
  // bus adc = 3 (12-bit, single sample, 532uS conversion time)
  // shunt adc = 3 (12-bit, single sample, 532uS conversion time)
  // mode = 7 (continuous conversion)
  curMonitor.configure(1, 3, 3, 7);
  // calibration of equations and device
  // shunt_val 		= value of shunt in Ohms
  // v_shunt_max 		= maximum value of voltage across shunt
  // v_bus_max 		= maximum voltage of bus
  // i_max_expected 	= maximum current draw of bus + shunt
  // default values are for a 0.25 Ohm shunt on a 5V bus with max current of 1A
  curMonitor.calibrate(0.1, 0.5, 30, 5);
}

void BspCurMon::process(void)
{
#if 0
  Serial.println("******************");

  Serial.print("raw shunt voltage: ");
  Serial.println(curMonitor.shuntVoltageRaw());

  Serial.print("raw bus voltage:   ");
  Serial.println(curMonitor.busVoltageRaw());

  Serial.println("--");

  Serial.print("shunt voltage: ");
  Serial.print(curMonitor.shuntVoltage() * 1000, 4);
  Serial.println(" mV");

  Serial.print("shunt current: ");
  Serial.print(curMonitor.shuntCurrent() * 1000, 4);
  Serial.println(" mA");

  Serial.print("bus voltage:   ");
  Serial.print(curMonitor.busVoltage(), 4);
  Serial.println(" V");

  Serial.print("bus power:     ");
  Serial.print(curMonitor.busPower() * 1000, 4);
  Serial.println(" mW");

  Serial.println(" ");
  Serial.println(" ");
#endif
}

uint32_t BspCurMon::getCurrent(void)
{
  return static_cast<uint32_t>(curMonitor.shuntCurrent()*100);
}

uint32_t BspCurMon::getVoltage(void)
{
  // return static_cast<uint16_t>(curMonitor.shuntVoltage()*10);
  return static_cast<uint32_t>(curMonitor.busVoltage()*10);
}

uint32_t BspCurMon::getPower(void)
{
  return static_cast<uint32_t>(curMonitor.busPower()*10);
}
