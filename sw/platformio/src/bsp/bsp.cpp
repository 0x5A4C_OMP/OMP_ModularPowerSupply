#include "bsp.hpp"

#define CLK_U 3
#define DIO_U 2
#define CLK_I 7
#define DIO_I 6

BspHeartBeat Bsp::bspHeartBeat;
BspSevenSegDisp Bsp::bspSevenSegDisp_U(CLK_U, DIO_U);
BspSevenSegDisp Bsp::bspSevenSegDisp_I(CLK_I, DIO_I);
BspCurMon Bsp::bspCurMon;

void Bsp::init(void)
{
  bspHeartBeat.init();
  bspSevenSegDisp_U.init();
  bspSevenSegDisp_I.init();
  bspCurMon.init();
}

void Bsp::process(void)
{
  bspCurMon.process();
}
