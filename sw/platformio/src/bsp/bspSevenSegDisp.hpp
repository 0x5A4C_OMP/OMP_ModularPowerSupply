#ifndef BSP_SEVENSEGDISP_HPP
#define BSP_SEVENSEGDISP_HPP

#include "TM1637.h"
#include "TM1637Display.hpp"

class BspSevenSegDisp
{
public:
  BspSevenSegDisp(const int clk, const int di);
  void init(void);
  void process(void);
  void display(void);
  void setValue(uint32_t value);
  void setUnit(uint8_t unit);
  void setDot(uint8_t pos);
protected:
private:
  // TM1637 tm1637;
  TM1637Display tm1637;
  uint8_t data[4];
  uint8_t dotPos;
};

#endif
