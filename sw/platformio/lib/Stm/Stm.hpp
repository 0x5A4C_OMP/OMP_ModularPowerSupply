#ifndef STM_HPP
#define STM_HPP

#include "stddef.h"

class Stm;

class St
{
public:
  St()
  {
  }

  St(Stm* parentStm): stm(parentStm)
  {
  }

  void setParentStm(Stm* parentStm)
  {
    stm = parentStm;
  }

  virtual void onEnter(void);
  virtual void process(void);
  virtual void onExit(void);
protected:
  Stm* stm;
private:
};

class Stm
{
public:
  Stm(): prevState(NULL), activeState(NULL)
  {
  };

  void transitionTo(St* nextState);
  void process(void);
protected:
private:
  St* prevState;
  St* activeState;
};

#endif
