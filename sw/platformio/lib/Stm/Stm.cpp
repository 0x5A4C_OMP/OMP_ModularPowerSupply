#include "Stm.hpp"

void St::onEnter(void)
{
}

void St::process(void)
{
}

void St::onExit(void)
{
}

void Stm::transitionTo(St* nextState)
{
  if(activeState != NULL)
    {
    activeState->onExit();
    prevState = activeState;
    }
  if(nextState != NULL)
    {
    activeState = nextState;
    activeState->onEnter();
    }
}

void Stm::process(void)
{
  if(activeState != NULL)
    {
      activeState->process();
    }
}
